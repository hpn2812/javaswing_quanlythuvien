/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author ADMIN
 */
public class BillEntity  {
    private  String id;
    private  LocalDate  date;
    private  String nameOfMember;
    private  int quantity;
    private  float price;
    private  String id_Product;
    private  String id_Employee;
    private  float totalMoney;

    public BillEntity() {
    }

    public BillEntity(String id, LocalDate date, String nameOfMember, int quantity, float price, String id_Product, String id_Employee, float totalMoney) {
        this.id = id;
        this.date = date;
        this.nameOfMember = nameOfMember;
        this.quantity = quantity;
        this.price = price;
        this.id_Product = id_Product;
        this.id_Employee = id_Employee;
        this.totalMoney = totalMoney;
    }

   

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

  

    public String getNameOfMember() {
        return nameOfMember;
    }

    public void setNameOfMember(String nameOfMember) {
        this.nameOfMember = nameOfMember;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getId_Product() {
        return id_Product;
    }

    public void setId_Product(String id_Product) {
        this.id_Product = id_Product;
    }

   

    public String getId_Employee() {
        return id_Employee;
    }

    public void setId_Employee(String id_Employee) {
        this.id_Employee = id_Employee;
    }

    public float getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(float totalMoney) {
        this.totalMoney = totalMoney;
    }

   
    
    
    
}
