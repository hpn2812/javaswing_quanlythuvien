/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.CategoryEntity;
import entities.EmployeeEntity;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class EmployeeDao {

    public static List<EmployeeEntity> getListEmployee() {
        List<EmployeeEntity> list = new ArrayList<>();
        ConnectDB cn = new ConnectDB();
        String sql = "select * from nhanvien";
        try {
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                EmployeeEntity em = new EmployeeEntity();
                em.setId(rs.getString(1));
                em.setName(rs.getString(2));
                em.setGender(rs.getString(3));
                em.setBirthday(rs.getDate(4).toLocalDate());
                em.setAddress(rs.getString(5));
                em.setPhoneNumber(rs.getString(6));
                em.setRole(rs.getString(7));
                list.add(em);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static boolean addEmployee(EmployeeEntity em, String sql) throws Exception {
        boolean kq = false;
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean deleteEmployee(String manv) throws Exception {
        boolean kq = false;
        String sql = "delete from nhanvien where manv= '" + manv + "'";
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean updateEmployee(String tennv, String gioitinh,LocalDate ngSinh, String diachi, String sdt, String chucvu, String manv) {
        boolean kq = false;
        String sql = "update nhanvien set tennv=N'" + tennv + "',gioitinh='" + gioitinh +"',namsinh='"+ngSinh+ "',diachi=N'" + diachi + "',sdt='" + sdt + "',chucvu='" + chucvu + "' where manv='" + manv + "'";
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        try {
            cn.Close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kq;
    }
    
    //Search
    public static ArrayList<EmployeeEntity> Search(String sql){
        ArrayList<EmployeeEntity> list = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
             EmployeeEntity em = new EmployeeEntity();
                em.setId(rs.getString(1));
                em.setName(rs.getString(2));
                em.setGender(rs.getString(3));
                em.setBirthday(rs.getDate(4).toLocalDate());
                em.setAddress(rs.getString(5));
                em.setPhoneNumber(rs.getString(6));
                em.setRole(rs.getString(7));
                list.add(em);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
